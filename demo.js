function extractTag(text) {
  function applySeparator(parts, separator) {
    return parts.flatMap(part =>
      part
        .split(separator)
        .map(part => part.trim())
        .filter(part => part.length > 0),
    )
  }
  let parts = [text]
  parts = applySeparator(parts, '\n')
  parts = applySeparator(parts, '#')
  parts = applySeparator(parts, ',')
  parts = applySeparator(parts, '，')
  parts = applySeparator(parts, '、')
  parts = parts.map(part => part.replace(/ /g, '-'))
  return new Set(parts)
}

let text = `   
#apple #pie, tasty, #good special offer #中文, #打字 
table-tennis, table tennis, football
#table-tennis, #table tennis, #football
table_tennis
table-tennis
tabletennis
`

console.log(extractTag(text))
