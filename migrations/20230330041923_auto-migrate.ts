import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  await knex.schema.alterTable('tag', table => {
    table.unique(['hashtag'])
  })
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.alterTable('tag', table => {
    table.dropUnique(['hashtag'])
  })
}
