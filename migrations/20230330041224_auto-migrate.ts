import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {

  if (!(await knex.schema.hasTable('post'))) {
    await knex.schema.createTable('post', table => {
      table.increments('id')
      table.integer('content').notNullable()
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('tag'))) {
    await knex.schema.createTable('tag', table => {
      table.increments('id')
      table.text('hashtag').notNullable()
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('post_tag'))) {
    await knex.schema.createTable('post_tag', table => {
      table.increments('id')
      table.integer('post_id').unsigned().notNullable().references('post.id')
      table.integer('tag_id').unsigned().notNullable().references('tag.id')
      table.timestamps(false, true)
    })
  }
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists('post_tag')
  await knex.schema.dropTableIfExists('tag')
  await knex.schema.dropTableIfExists('post')
}
