import { Router } from 'express'
import { db } from './db'
import { extractTag } from './tag'

export let postRoutes = Router()

let insert_post = db.prepare(/* sql */ `
insert into post (content) values (?)
returning id
`)

let select_tag_id = db
  .prepare(
    /* sql */ `
select id from tag where hashtag = ?
`,
  )
  .pluck()

let insert_tag = db.prepare(/* sql */ `
insert into tag (hashtag) values (?)
returning id
`)

let insert_post_tag = db.prepare(/* sql */ `
insert into post_tag (post_id, tag_id) values (?, ?)
`)

postRoutes.post('/post', (req, res, next) => {
  let tags = extractTag(req.body.hashtags)
  let result = insert_post.run(req.body.content)
  let post_id = result.lastInsertRowid

  for (let tag of tags) {
    let tag_id = select_tag_id.get(tag)
    if (!tag_id) {
      tag_id = insert_tag.run(tag).lastInsertRowid
    }
    insert_post_tag.run(post_id, tag_id)
  }

  res.json({ post_id })
})

let select_hashtags = db.prepare(/* sql */ `
select
  tag.id
, tag.hashtag
, count(post_tag.post_id) as count
from tag
inner join post_tag on post_tag.tag_id = tag.id
group by tag.id
order by count desc
`)

postRoutes.get('/tags', (req, res, next) => {
  let hashtags = select_hashtags.all()
  res.json({ hashtags })
})
