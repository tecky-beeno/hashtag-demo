import express from 'express'
import { print } from 'listening-on'
import { postRoutes } from './post'

let app = express()

app.use(express.static('public'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.use(postRoutes)

let port = 8100
app.listen(port, () => {
  print(port)
})
